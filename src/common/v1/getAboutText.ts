import { NextFunction, Request, Response } from "express";
import mongoose, { Document } from "mongoose";
import textVarParser from "../../helpers/textVarParser";
import About from "../../Models/v1/About";
import { errorLogger } from "../../helpers/errorLogger";

interface AboutDocument {
  text: string;
  _id: mongoose.Types.ObjectId;
}

const getAboutText = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let aboutText: Document<AboutDocument>[] = await About.find().limit(1);

  if (aboutText.length < 0) {
    res.status(404).json({ error: "There was in internal server error" });
    errorLogger(
      "There was in internal server error",
      req.headers["x-request-uuid"] as string
    );
    return next();
  }

  let parsedText = textVarParser(
    ((aboutText[0] as unknown) as AboutDocument).text
  );

  res.send({ data: parsedText });
  return next();
};

export default getAboutText;
