import { NextFunction, Request, Response } from "express";
import { errorLogger } from "../../helpers/errorLogger";
import About from "../../Models/v1/About";

const editAboutText = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { text } = req.body;

  if (text === undefined) {
    res
      .status(400)
      .json({ error: "Pleas provide a text attribute in the body" });
    errorLogger("No text provided", req.headers["x-request-uuid"] as string);
    return next();
  }

  let abouts = await About.find();

  if (abouts === null) {
    const newAboutEntry = new About({
      text,
      updatedAt: new Date(),
    });

    newAboutEntry
      .save()
      .then((aboutEntry) => {
        res.json(aboutEntry);
        return next();
      })
      .catch((err) => {
        res.status(400).json({ error: err });
        errorLogger(err, req.headers["x-request-uuid"] as string);
        return next();
      });
  } else {
    let id = abouts[0]._id;

    const updatedAbout: any = {};
    if (text) updatedAbout.text = text;
    updatedAbout.updatedAt = new Date();

    About.findByIdAndUpdate(id, { $set: updatedAbout }, { new: true })
      .then((newAbout) => {
        res.json(newAbout);
        return next();
      })
      .catch((err) => {
        res.json(err);
        errorLogger(err, req.headers["x-request-uuid"] as string);
        return next();
      });
  }
};

export default editAboutText;
