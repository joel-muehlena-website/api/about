import mongoose from "mongoose";
const Schema = mongoose.Schema;

const AboutSchema = new Schema({
  text: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

const About = mongoose.model("about", AboutSchema);
export default About;
