import express from "express";
import getAboutText from "../../common/v1/getAboutText";
import editAboutText from "../../common/v1/editAboutText";

const router = express.Router();

router.get("/", getAboutText);
router.post("/", editAboutText);

export default router;
