import mongoose from "mongoose";

export default function connectToMongoose() {
  const mongoUri = `mongodb+srv://${process.env.MONGOUSERNAME}:${process.env.MONGODBPASSWORD}@${process.env.MONGOCONNECTIONSTRING}`;
  const mongoOptions = {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  };

  mongoose
    .connect(mongoUri, mongoOptions)
    .then((_) => {
      console.log("Connected to MongoDB Atlas Cluster");
    })
    .catch((err) => {
      console.log(err);
    });
}
