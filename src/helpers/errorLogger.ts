//import { KafkaLogger } from "./kafka-logger";

export const errorLogger = (error: string, uuid: string) => {
  console.log("[TMP ERROR LOG]", error, uuid);
  /*const brokers = process.env.KAFKA_BROKERS?.split(",");
  const kafkaLogger = new KafkaLogger(brokers!);

  const value = {
    requestId: uuid,
    controller: "about-service",
    timestamp: new Date().toISOString(),
    error,
  };
  kafkaLogger.produceMessage("request-logging", [
    { key: uuid, value: JSON.stringify(value) },
  ]);*/
};

export const runtimeErrorLogger = (err: Error) => {
  console.log("[TMP RUNTIME ERROR LOG]", err);
  /*const kafkaLogger = new KafkaLogger([
    "192.168.0.127:9091",
    "192.168.0.127:9092",
    "192.168.0.127:9093",
  ]);
  kafkaLogger.produceMessage("about-service-log", [
    {
      key: "about-service-log",
      value: `[${Date.now}] ${err.message}`,
    },
  ]);*/
};
