import { NextFunction, Request, Response } from "express";
//import { KafkaLogger } from "./kafka-logger";

export const endRequestLogger = () => {
  return (req: Request, _: Response, next: NextFunction) => {
    if (req.headers["x-request-uuid"]) {
      /*let uuid = req.headers["x-request-uuid"] as string;
      const brokers = process.env.KAFKA_BROKERS?.split(",");
      const kafkaLogger = new KafkaLogger(brokers!);
      const value = {
        timestamp: new Date().toISOString(),
        requestId: uuid,
        controller: "about-service",
        requestedPath: req.path,
        method: req.method,
        status: "EXIT_CONTROLLER",
      };
      kafkaLogger.produceMessage("request-logging", [
        { key: uuid, value: JSON.stringify(value) },
      ]);*/
    }
    next();
  };
};
