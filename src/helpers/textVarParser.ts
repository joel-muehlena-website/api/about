const calculateAlter = (gebDate: Date): number => {
  let ageDifMs = Date.now() - gebDate.getTime();
  let ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
};

const textVarParser = (text: string) => {
  const alter = calculateAlter(new Date("11-08-2001"));

  return text.replace("${alter}", alter.toString());
};

export default textVarParser;
