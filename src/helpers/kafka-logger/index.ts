import { CompressionTypes, Kafka, logLevel, Message, Producer } from "kafkajs";

export class KafkaLogger {
  private kafka: Kafka;
  private producer: Producer;

  constructor(brokers: Array<string>) {
    this.kafka = new Kafka({
      brokers: [...brokers],
      logLevel: logLevel.ERROR,
    });
    this.producer = this.kafka.producer();
  }

  public async produceMessage(topic: string, messages: [Message]) {
    await this.producer.connect();

    try {
      await this.producer.send({
        topic,
        messages,
        compression: CompressionTypes.GZIP,
      });
    } catch (err) {
      console.log("[KAFKA Error] ", err);
    }

    await this.producer.disconnect();
  }
}
