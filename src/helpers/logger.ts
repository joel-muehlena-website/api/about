import { NextFunction, Request, Response } from "express";
//import { KafkaLogger } from "./kafka-logger";

const parsePath = (p: String): [string, string] => {
  let path = p.slice(1);

  let versionString = "";

  for (let i = 0; i < path.length; ++i) {
    if (path.charAt(i) === "/") {
      break;
    }
    versionString += path.charAt(i);
  }

  path = path.replace(versionString, "");

  switch (versionString) {
    case "v1":
      return ["AVersion 1", path];
    default:
      return ["No version found", path];
  }
};

const logger = () => {
  return (req: Request, _: Response, next: NextFunction) => {
    const d = new Date();
    const ye = new Intl.DateTimeFormat("en", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour12: false,
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    }).format(d);

    const parsedPath = parsePath(req.path);

    console.log(
      "\x1b[32m",
      `[Request-Logger] ${ye} : ${parsedPath[1]} ${parsedPath[0]} id:${req.headers["x-request-uuid"]}`
    );
    console.log(
      "\x1b[32m",
      `[LOG-HEADER-DEV] is-auth: ${req.headers["x-auth-filter"]}`
    );

    if (req.headers["x-request-uuid"]) {
      /* let uuid = req.headers["x-request-uuid"] as string;
      const brokers = process.env.KAFKA_BROKERS?.split(",");
      const kafkaLogger = new KafkaLogger(brokers!);
      const value = {
        timestamp: new Date().toISOString(),
        requestId: uuid,
        controller: "about-service",
        requestedPath: req.path,
        method: req.method,
        status: "START_CONTROLLER",
      };
      kafkaLogger.produceMessage("request-logging", [
        { key: uuid, value: JSON.stringify(value) },
      ]);*/
    }

    next();
  };
};

export default logger;
