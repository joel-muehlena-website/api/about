import express, { NextFunction, Request, Response } from "express";
import connectToMongoose from "./config/mongoDBSetup";
import dotenv from "dotenv";
import logger from "./helpers/logger";
import { endRequestLogger } from "./helpers/endRequestLogger";
import { configureServerAndEnv } from "@joel-muehlena-website/jm-config-server-addon";
import path from "path";
import about from "./routes/v1/about";
import * as fs from "fs";
import { runtimeErrorLogger } from "./helpers/errorLogger";
import bodyParser from "body-parser";

const configServer = `http://${
  process.env.NODE_ENV === "production"
    ? "config-server.default.svc.cluster.local"
    : "172.30.204.237"
}:8081/config?filename=about:${
  process.env.NODE_ENV === "production" ? "prod" : "dev"
}`;

(async () => {
  try {
    await configureServerAndEnv(configServer, path.join(__dirname, "/.env"));
  } catch (err) {
    console.log("Error fetching config server");
    runtimeErrorLogger(err);
    process.exit(err);
  }
})();

let count = 0;
let i = setInterval(() => {
  if (count > 5000) {
    console.log("Config Timeout");
    process.exit(-1);
  }
  fs.access(path.join(__dirname, "/.env"), fs.constants.F_OK, (err) => {
    if (!err) {
      clearInterval(i);
      runApp();
    } else {
      count++;
    }
  });
}, 500);

const runApp = () => {
  dotenv.config({ path: path.join(__dirname, "/.env") });

  const PORT = process.env.PORT;

  const app = express();

  connectToMongoose();

  app.get("/healthz", (_: Request, res: Response) => {
    res.sendStatus(200);
  });

  app.use(logger());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  app.use((req: Request, res: Response, next: NextFunction) => {
    res.locals.reqUUID = req.headers["x-request-uuid"];
    next();
  });

  app.use("/v1/about", about);

  app.use(endRequestLogger());

  app.listen(PORT, () => {
    console.log(`Server läuft auf Port ${PORT}`);
  });
};
