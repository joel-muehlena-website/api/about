FROM node:15.8.0-alpine3.12

ARG APP_IS_STAGING_ARG=false
ENV APP_IS_STAGING=$APP_IS_STAGING_ARG

ARG NPM_REGISTRY_TOKEN

ENV NODE_ENV="production"

WORKDIR /server

COPY . .

RUN npm i

EXPOSE 80

CMD ["node", "build/index"]
